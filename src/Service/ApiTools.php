<?php

namespace Drupal\git_repo_info\Service;

use Drupal;
use Exception;
use Gitlab\Client;
use Gitlab\ResultPager;

/**
 * Class ApiTools
 *
 * @package Drupal\git_repo_info\Service
 */
class ApiTools {

  protected $connection;
  protected $config;
  protected $token;
  protected $client;

  /**
   * ApiTools constructor.
   *
   * @param $connection
   */
  public function __construct($connection) {
    $this->connection = $connection;
    $this->config = Drupal::config('git_repo_info.settings');
    $this->token = $this->config->get('git_repo_info.private_token');
    $this->client = $this->getClient($this->token);
  }

  /**
   * @param $token
   *
   * @return \Gitlab\Client
   */
  protected function getClient($token = FALSE) {
    $token = is_string($token) ? $token : $this->token;
    $client = new Client();
    return $client->authenticate($token, Client::AUTH_OAUTH_TOKEN);
  }

  /**
   * @param $token
   *
   * @return bool
   */
  public function validateCredentials($token = FALSE) {
    $client = $this->getClient($token);

    $this->getProjects();

    try {
      $client->projects()->all(['membership' => TRUE]);
      return TRUE;
    }
    catch(Exception $e) {
      return FALSE;
    }

  }

  public function getProjects($only_of_user = TRUE) {
    $data_to_keep = [
      'id',
      'description',
      'name',
      'web_url',
      'last_activity_at',
      'archived',
      'visibility',
    ];

    $pager = new ResultPager($this->client);
    $projects_arr = [];

    try{
//      $projects = $this->client->projects()->all(['membership' => $only_of_user]);
      $projects_arr = $pager->fetchAll($this->client->projects(), 'all', [['membership' => $only_of_user]]);
//      return $projects_arr;
    }
    catch (Exception $e) {
      return FALSE;
    }

    if(!empty($projects_arr)) {
      foreach($projects_arr as $project_key => $project) {
        foreach($project as $field_key => $field_value) {
          if (!in_array($field_key, $data_to_keep))
            unset($projects_arr[$project_key][$field_key]);
        }
      }


//      kint($projects_arr);
//      die('End of die. 2');
    }

    return $projects_arr;

  }
}
