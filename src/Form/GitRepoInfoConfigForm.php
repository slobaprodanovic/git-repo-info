<?php

namespace Drupal\git_repo_info\Form;

use Drupal;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class GitRepoInfoConfigForm
 * @package Drupal\git_repo_info\Form
 */
class GitRepoInfoConfigForm extends ConfigFormBase {

  /**
   * {@inheritDoc}
   *
   * @return string
   */
  public function getFormId() {
    return 'git_repo_info_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('git_repo_info.settings');

    $form['gitlab_container'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Gitlab'),
    ];

    $form['gitlab_container']['private_token'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Private token'),
      '#default_value' => $config->get('git_repo_info.private_token'),
      '#required' => TRUE,
    );

    $form['gitlab_container']['actions']['validate'] = array(
      '#type' => 'submit',
      '#name' => 'validate',
      '#value' => $this->t('Validate'),
    );

    $header = [
      ['data' => t('ID'), 'field' => 'id', 'sort' => 'asc'],
      ['data' => t('Name'), 'field' => 'name', 'sort' => 'asc'],
      ['data' => t('Description'), 'field' => 'description', 'sort' => 'asc'],
      ['data' => t('Link'), 'field' => 'web_url', 'sort' => 'asc'],
      ['data' => t('Last activity'), 'field' => 'last_activity_at', 'sort' => 'asc'],
      ['data' => t('Archived'), 'field' => 'archived', 'sort' => 'asc'],
      ['data' => t('Expires'), 'field' => 'expires', 'sort' => 'asc'],
      ['data' => t('Visibility'), 'field' => 'visibility', 'sort' => 'asc'],
      ['data' => t('Operation')],
    ];

    $form['gitlab_container']['gitlab_projects_info'] = array(
      '#type' => 'table',
      '#header' => $header,
      '#empty' => t('There are no items yet.'),
    );

//    $data_to_keep = [
//      'id',
//      'description',
//      'name',
//      'web_url',
//      'last_activity_at',
//      'archived',
//      'visibility',
//    ];



    $gitlab_api_service = Drupal::service('git_repo_info.api_tools');
    $data = $gitlab_api_service->getProjects();
//    kint($data);
//    die('End of die.');

    $number_order = 0;
    foreach($data as $single_data) {
      $number_order++;
//      kint($single_data);
//      die('End of die 2.');
      $form['gitlab_container']['gitlab_projects_info'][$single_data['id']]['no'] = [
        '#type' => 'item',
        '#title' => 'Title',
        '#title_display' => 'invisible',
        '#markup' => $number_order,


//        '#type' => 'item',
//        '#size' => 2,
//        '#default_value' => $number_order,
//        '#required' => TRUE,
//        '#attributes' => [
//          'readonly' => 'readonly',
//        ],
      ];

      $form['gitlab_container']['gitlab_projects_info'][$single_data['id']]['name'] = [
        '#type' => 'item',
        '#title' => 'Title',
        '#title_display' => 'invisible',
        '#markup' => $single_data['name'],
//        '#type' => 'textfield',
//        '#size' => 20,
//        '#default_value' => $single_data['name'],
//        '#required' => TRUE,
//        '#attributes' => [
//          'readonly' => 'readonly',
//        ],
      ];

      $form['gitlab_container']['gitlab_projects_info'][$single_data['id']]['description'] = [
        '#type' => 'item',
        '#title' => 'Title',
        '#title_display' => 'invisible',
        '#markup' => $single_data['description'],
//        '#type' => 'textarea',
//        '#size' => 100,
//        '#default_value' => $single_data['description'],
//        '#required' => TRUE,
//        '#attributes' => [
//          'readonly' => 'readonly',
//        ],
      ];

      $form['gitlab_container']['gitlab_projects_info'][$single_data['id']]['status'] = [
        '#type' => 'checkbox',
//        '#default_value' => $single_data['status'],
      ];
    }




    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Perform validation.
    $triggering_element = $form_state->getTriggeringElement();
    $private_token_input = $form_state->getValue('private_token');

    if ($triggering_element['#name'] === 'validate') {
      $this->validateCredentials($private_token_input);
    }
    else {
      // Save submitted data.
      $config = $this->config('git_repo_info.settings');
      $config->set('git_repo_info.private_token', $private_token_input);
      $config->save();

      return parent::submitForm($form, $form_state);
    }

  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {

    return [
      'git_repo_info.settings',
    ];
  }

  /**
   * @param $token
   */
  protected function validateCredentials($token) {
    $gitlab_api_service = Drupal::service('git_repo_info.api_tools');

    if ($gitlab_api_service->validateCredentials($token)) {
      Drupal::messenger()->deleteAll();
      Drupal::messenger()->addMessage('Valid token!', 'status');
    }
    else {
      Drupal::messenger()->addMessage('Invalid token!', 'warning');
    }
  }
}
